# distances

A package for calculating different distance measures. 

# Installation 
The project can be cloned locally using the following command:
$ git clone <path>

#Usage
Currently the following distance measures are implemented in the package: - Euclidean
distance. 

# Tests
The package is being tested using pytest. To run the test suite use the command:
$ pytest test_euclidean

#License
The package is under the MIT license.

a python package for calculating various distance measures.
